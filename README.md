## Duke osquery configuration scripts

This is an ansible file and a wrapper shell script used to configure osquery to report to Duke's fleet servers.

The steps to run this are:

1. Ensure you have ansible installed for your distribution
2. Clone this repository
3. Execute the `install.sh` script

If you use a deb or rpm based distribution, this will install the official packages in those formats from upstream.
Other distributions will use upstream binary tarballs.

You will be responsible for further updates to the actual osquery software as needed. This script may or may not
be updated periodically with new versions, so you should ensure you have some way to manage the patch cycle for osquery itself.

If you'd rather not use this script, you can install manually using the configurations found in this repository. Of note,
ensure you use the "flags" file provided above and the enrollment_secret.

If your distributions have officially maintained OSquery packages, you may wish to track them instead of the upstream. You
can either use this repo first and afterward replace the package with one from your preferred source, or use this repo as a base
that you customise with your preferred installation source.

PRs to automate these options are welcome :)
