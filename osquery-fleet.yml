---
  - hosts: all

    vars:
      clone_dir: /root/osquery-fleet-client-linux
      service_file_source: osqueryd-duke-usrlocal.service
      service_file: /etc/systemd/system/osqueryd-duke.service
      use_tarball: true
      enrollment_secret: enrollment_secret
      cert_file: fleet.oit.duke.edu.pem
      flag_file: kolide.flags
      package_prefix: osquery-
      package_version: 2.10.2_1
      package_suffix: .linux_x86_64.tar.gz
      base_url: https://pkg.osquery.io/linux
      package_checksum: sha256:7871ad268bccc9a84760c365ce3f1d1832ef75e06d5d27273cb6237980c50d43

    tasks:
      - name: OSQUERY | Clone support repo
        git:
          repo: https://gitlab.oit.duke.edu/devil-ops/osquery-fleet-client-linux.git
          version: master
          dest: "{{ clone_dir }}"
          depth: 1
      - name: Include package Variables
        include_vars: "{{ ansible_pkg_mgr }}.yml"
        when: ansible_pkg_mgr == 'dnf' or ansible_pkg_mgr == 'yum' or ansible_pkg_mgr == 'apt'
      - name: OSQUERY | Download binary
        register: binaryfile
        get_url:
          url: "{{ base_url }}/{{package_prefix}}{{package_version}}{{package_suffix}}"
          dest: "{{ clone_dir }}"
          checksum: "{{ package_checksum }}"
      - name: OSQUERY | Install RPM binary package with yum
        yum:
          name: "{{ clone_dir }}/{{package_prefix}}{{package_version}}{{package_suffix}}"
          state: present
        when: ansible_pkg_mgr == 'yum'
      - name: OSQUERY | Install RPM binary package with dnf
        dnf:
          name: "{{ clone_dir }}/{{package_prefix}}{{package_version}}{{package_suffix}}"
          state: present
        when: ansible_pkg_mgr == 'dnf'
      - name: OSQUERY | Install apt binary package
        apt:
          deb: "{{ clone_dir }}/{{package_prefix}}{{package_version}}{{package_suffix}}"
          state: present
        when: ansible_pkg_mgr == 'apt'
      - name: OSQUERY | make target directory for tarball
        file:
          path: /usr/local/osquery
          state: directory
          mode: 755
        when: use_tarball == true
      - name: OSQUERY | make config directory for tarball
        file:
          path: /etc/osquery
          state: directory
          mode: 755
        when: use_tarball == true
      - name: OSQUERY | explode tarball for other distributions
        command: "tar -xf {{ clone_dir }}/{{package_prefix}}{{package_version}}{{package_suffix}} -C /usr/local/osquery"
        when: use_tarball == true
      - name: OSQUERY | fix perms on install dir
        file:
          path: /usr/local/osquery
          owner: root
          group: root
          recurse: true
        when: use_tarball == true
      - name: OSQUERY | Copy enrollment secret
        copy:
          src: "{{ clone_dir }}/{{ enrollment_secret }}"
          dest: /etc/osquery/enrollment_secret
      - name: OSQUERY | Copy cert
        copy:
          src: "{{ clone_dir }}/{{ cert_file }}"
          dest: /etc/osquery/kolide.crt
      - name: OSQUERY | Copy flags
        copy:
          src: "{{ clone_dir }}/{{ flag_file }}"
          dest: /etc/osquery/kolide.flags
      - name: OSQUERY | Add service file
        register: servicefile
        copy:
          src: "{{ clone_dir }}/{{ service_file_source }}"
          dest: "{{ service_file }}"
          owner: root
          group: root
          mode: 0444
      - name: OSQUERY | update systemd
        command: systemctl daemon-reload
        when: servicefile.changed
      - name: OSQUERY | enable service
        service:
          name: osqueryd-duke.service
          state: started
          enabled: yes
